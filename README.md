# Projet Infra & SI
Guillaume Mareschal, Audrey Flambard
---

Mise en place d'un serveur de stream sur une machine Linux (Ubuntu).

## Live avec OBS et nginx
---
### 1. Mise en place de Nginx
Dans un premier temps il faut installer et configurer nginx pour pouvoir streamer avec OBS.
Il faut donc installer nginx avec la commande 
```sudo apt install nginx```
On a aussi besoin du module RTMP pour que nginx puisse gérer notre stream 
```
sudo add-apt-repository universe
sudo apt install libnginx-mod-rtmp
```
Il suffit maintenant de modifier le fichier de configuration :
```sudo nano /etc/nginx/nginx.conf```
Aller en bas du fichier et ajouter le code suivant :
```
rtmp {
        server {
                    listen 1935;
                    chunk_size 4096;
                    
                    application_live {
                        live on;
                        record off;
                    }
        }
}
```
Il ne reste plus qu'à redémarrer nginx pour que les modifications fassent effet
```sudo systemctl restart nginx```

### 2. Mise en place d'OBS
Il faut tout d'abord installer OBS avec la commande
```sudo apt install obs-studio```
Une fois l'installation terminée il faut choisir une source à streamer, pour ce projet nous avons choisi de faire un partage d'écran. Pour ce faire il faut simplement cliquer sur le bouton +, en dessous de **Source** choisir **Screen Capture**.
Maintenant il ne reste plus qu'à envoyer notre partage d'écran sur notre serveur. 
Dans OBS cliquer sur **File > Settings**. Cliquer sur la section **Stream** puis mettre le **Stream Type** sur **Custom**.
Dans la section URL ajouter l'adresse avec le préfixe rtmp:// puis l'IP de votre serveur de stream suivi par /live. ```rtmp://Adresse-IP/live```. On peut choisir de mettre une clé de stream ou non.
Cliquer sur **Apply** puis **OK**.
Maintenant qu'OBS est configuré pour envoyer le stream sur le serveur cliquer sur **Start Streaming**. Si tout est bon alors le stream se mettra en route sans encombre.

### 3. Regarder le stream
Pour regarder le stream nous avons besoin de VLC Media Player. Après l'installation il suffit de cliquer sur **Media > Open Network Stream**, d'entrer l'adresse du stream (la même que celle utilisée dans OBS) et cliquer sur **Play**.

### 4. Sauvegarde du stream
Pour pouvoir sauvegarder notre stream il y a deux manières : passer directement par les Settings d'OBS ou par le fichier de configuration de Nginx.
Dans le fichier de configuration de Nginx, il suffit de rajouter ces deux lignes dans ```application live``` :
```
rtmp {
        server {
                    listen 1935;
                    chunk_size 4096;
                    
                    application_live {
                        live on;
                        record off;
                        record-path /var/www/html/recordings;
                        record_unique on;
                    }
        }
}
```
En s'assurant que Nginx ait les droits d'écriture dans le dossier indiqué et que ce dossier existe bien.

## Site web
---
Nous devions automatiser l'utilisation de ce stream : le mettre sur un site web pour y avoir accès facilement. Mais n'ayant pas réussi cette partie, nous n'avons pas de doc à fournir. 
Nous avons néanmoins mis un système de monitoring afin de voir combien de personnes utilisent le serveur et par la même occasion visualiser l'état général de la machine. Pour ça nous utilisons Netdata :
```
apt install curl
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
NOTE: Running apt-get update and updating your APT caches ...
apt-get update 
apt-get install autoconf autoconf-archive autogen automake cmake gcc git libelf-dev libjson-c-dev libjudy-dev liblz4-dev libmnl-dev libssl-dev libtool libuv1-dev make pkg-config uuid-dev zlib1g-dev 


Press ENTER to run it > 
```
Après l'installation il faut modifier le fichier de configuration Nginx afin d'autoriser le module **stub_status**.
```sudo nano /etc/nginx/nginx.conf```
Ajouter la configuration suivante dans le block '**server{}**' :
```
    location /stub_status {
        stub_status;
        # Security: Only allow access from the IP below.
        allow 127.0.0.1;
        # Deny anyone else
        deny all;
    }
```
Sauvegarder et quitter.
Tester la configuration nginx pour d'assurer qu'il n'y ait pas d'erreur : 
```
nginx -t
systemctl restart nginx
```
Le module 'stub_status' de nginx a été mis en place, on peut le vérifier en utilisant la commande suivante :
```curl http://127.0.0.1/stub_status```

Nous voilà avec un monitoring de notre page web.
